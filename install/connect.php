<?php
require("install/mysql.php");
function connect_db()
{
	try
	{
    	$bd=new PDO("mysql:host=".BD_HOST."; dbname=".BD_BASE."; charset =UTF8", BD_USER, BD_PASSWORD, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));    

    	return $bd;
	}catch (Exception $e)
	{
   	 	echo "<p> Problème lors de la connexion à la base de donnée.</p>";
    	exit();
	}
}


?>