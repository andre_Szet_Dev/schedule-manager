<!DOCTYPE html>
<html lang="fr">
  <head>
    <title> Gestion d'heures d'agents </title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="description" content="Exemple d'AJaX"/>
    <meta name="author" content="Cyril Rabat"/>
  </head>
  <body>
    <h1>Création de la base de données des horaires agents</h1>
<?php
// Configuration pour la base de données
include("mysql.php");

// Connexion au SGBD (sans base de données)
try {  
   $BD = new PDO("mysql:host=".BD_HOST.";charset=UTF8", BD_USER, BD_PASSWORD);
} catch(Exception $e) {
    echo "<p> Problème de connexion au SGBD. </p>";
    exit();
}

// Création de la base de données
$erreur = false;
if(($BD->prepare("DROP DATABASE IF EXISTS `".BD_BASE."`;")->execute() &&
    $BD->prepare("CREATE DATABASE IF NOT EXISTS `".BD_BASE."` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;")->execute()))
    echo "<p> Création de la base de données ".BD_BASE." réussie. </p>";
else {
    $erreur = true;
    echo "<p> Problème lors de la création de la base de données ".BD_BASE.". </p>";
}

if(!$erreur) {
    // Connexion à la base de données
    try 
    {  
        $BD = new PDO("mysql:host=".BD_HOST.";dbname=".BD_BASE.";charset=UTF8", BD_USER, BD_PASSWORD,array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
    } catch(Exception $e) 
    {
        echo "<p> Problème de connexion à la base de données. </p>";
        exit();
    }    

    echo "<h1>Création de la table ".TABLE_ROLE ."</h1>";
    
    // Création de la table Role

    $t_role = "CREATE  TABLE ".TABLE_ROLE."(
                id INT PRIMARY KEY,
                nom varchar (32) NOT NULL
                ) 
                ENGINE=InnoDb DEFAULT CHARSET=utf8;";
                
    if($BD->prepare($t_role)->execute())
    {
        echo "<p> Création de la table ".TABLE_ROLE." réussie. </p>";
    }
    else {
        $erreur = true;
        echo "<p> Problème lors de la création de la table ".TABLE_ROLE.". </p>";   
    }
    
    echo "<h1>Création de la table".TABLE_AGENTS ."</h1>";
    
    // Création de la table Agent

    $t_agent = "CREATE  TABLE ".TABLE_AGENTS."(
                id INT AUTO_INCREMENT PRIMARY KEY,
                nom varchar(32) NOT NULL,
                prenom varchar(32) NOT NULL,
                mail varchar(100) NOT NULL UNIQUE,
                pass varchar(40) character set ascii not null,
                poste varchar (50) NOT NULL,
                id_role int NOT NULL,
                CONSTRAINT role_agent_fk FOREIGN KEY (id_role)
                REFERENCES ".TABLE_ROLE."(id)
                ) 
                ENGINE=InnoDb DEFAULT CHARSET=utf8;";
                
    if($BD->prepare($t_agent)->execute()){
        echo "<p> Création de la table ".TABLE_AGENTS." réussie. </p>";
    }
    else {
        $erreur = true;
        echo "<p> Problème lors de la création de la table ".TABLE_AGENTS.". </p>";   
    }

    // Création de la table Nature

    echo "<h1>Création de la table".TABLE_NATURE ."</h1>";
    

    $t_nature = "CREATE  TABLE ".TABLE_NATURE."(
                id INT AUTO_INCREMENT PRIMARY KEY,
                nature varchar (50) NOT NULL
                ) 
                ENGINE=InnoDb DEFAULT CHARSET=utf8;";
                
    if($BD->prepare($t_nature)->execute()){
        echo "<p> Création de la table ".TABLE_NATURE." réussie. </p>";
    }
    else {
        $erreur = true;
        echo "<p> Problème lors de la création de la table ".TABLE_NATURE.". </p>";   
    }

    // Création de la table Tache

    echo "<h1>Création de la table".TABLE_TACHE ."</h1>";
    

    $t_tache = "CREATE  TABLE ".TABLE_TACHE."(
                id INT AUTO_INCREMENT PRIMARY KEY,
                tache varchar (50) NOT NULL
                ) 
                ENGINE=InnoDb DEFAULT CHARSET=utf8;";
                
    if($BD->prepare($t_tache)->execute()){
        echo "<p> Création de la table ".TABLE_TACHE." réussie. </p>";
    }
    else {
        $erreur = true;
        echo "<p> Problème lors de la création de la table ".TABLE_TACHE.". </p>";   
    }

    // Création de la table Type

    echo "<h1>Création de la table".TABLE_TYPE ."</h1>";
    

    $t_type = "CREATE  TABLE ".TABLE_TYPE."(
                id INT AUTO_INCREMENT PRIMARY KEY,
                type varchar (50) NOT NULL
                ) 
                ENGINE=InnoDb DEFAULT CHARSET=utf8;";
                
    if($BD->prepare($t_type)->execute()){
        echo "<p> Création de la table ".TABLE_TYPE." réussie. </p>";
    }
    else {
        $erreur = true;
        echo "<p> Problème lors de la création de la table ".TABLE_TYPE.". </p>";   
    }

    // Création de la table Lieux

      echo "<h1>Création de la table ".TABLE_LIEUX ."</h1>";
    
    $t_lieux = "CREATE  TABLE ".TABLE_LIEUX."(
                id INT AUTO_INCREMENT PRIMARY KEY,
                lieu varchar (50) NOT NULL,
                adresse varchar (250) NOT NULL,
                tel char(10) NOT NULL UNIQUE
                ) 
                ENGINE=InnoDb DEFAULT CHARSET=utf8;";
                
    if($BD->prepare($t_lieux)->execute()){
        echo "<p> Création de la table ".TABLE_LIEUX." réussie. </p>";
    }
    else {
        $erreur = true;
        echo "<p> Problème lors de la création de la table ".TABLE_LIEUX.". </p>";   
    }

        echo "<h1>Création de la table ".TABLE_LIEUX ."</h1>";
    
    $t_periode = "CREATE  TABLE ".TABLE_PERIODE."(
                id INT AUTO_INCREMENT PRIMARY KEY,
                nom_periode varchar (50) NOT NULL,
                date_debut date  NOT NULL,
                date_fin date NOT NULL,
                annee year(4) NOT NULL
                ) 
                ENGINE=InnoDb DEFAULT CHARSET=utf8;";
                
    if($BD->prepare($t_periode)->execute()){
        echo "<p> Création de la table ".TABLE_PERIODE." réussie. </p>";
    }
    else {
        $erreur = true;
        echo "<p> Problème lors de la création de la table ".TABLE_PERIODE.". </p>";   
    }
      echo "<h1>Création de la table ".TABLE_CONTRATS ."</h1>";
    
    // Création de la table Contrat

    $t_contrat = "CREATE  TABLE ".TABLE_CONTRATS."(
                id INT AUTO_INCREMENT PRIMARY KEY,
                type varchar(10),
                date_debut date NOT NULL,
                date_fin date,
                id_agent INT NOT NULL,
                CONSTRAINT agent_contrat_fk FOREIGN KEY (id_agent)
                REFERENCES ".TABLE_AGENTS."(id)
                ) 
                ENGINE=InnoDb DEFAULT CHARSET=utf8;";
                
    if($BD->prepare($t_contrat)->execute()){
        echo "<p> Création de la table ".TABLE_CONTRATS." réussie. </p>";
    }
    else {
        $erreur = true;
        echo "<p> Problème lors de la création de la table ".TABLE_CONTRATS.". </p>";   
    
    }
   echo "<h1>Création de la table ".TABLE_HEURES_PREVUES ."</h1>";
    
    // Création de la table heuresPrevues

    $t_prevues = "CREATE  TABLE ".TABLE_HEURES_PREVUES."(
                id INT AUTO_INCREMENT PRIMARY KEY,
                jour date  NOT NULL,
                debut datetime NOT NULL,
                fin datetime NOT NULL,
                validee bool,
                id_tache INT NOT NULL,
                id_agent INT NOT NULL,
                id_lieu INT NOT NULL,
                id_type INT NOT NULL,
                id_periode INT NOT NULL,
                CONSTRAINT prevues_tache_fk FOREIGN KEY (id_tache)
                REFERENCES ".TABLE_TACHE."(id),
                CONSTRAINT prevues_agent_fk FOREIGN KEY (id_agent)
                REFERENCES ".TABLE_AGENTS."(id),
                CONSTRAINT prevues_lieu_fk FOREIGN KEY (id_lieu)
                REFERENCES ".TABLE_LIEUX."(id),
                CONSTRAINT prevues_type_fk FOREIGN KEY (id_type)
                REFERENCES ".TABLE_TYPE."(id),
                CONSTRAINT prevues_periode_fk FOREIGN KEY (id_periode)
                REFERENCES ".TABLE_PERIODE."(id)
                ) 
                ENGINE=InnoDb DEFAULT CHARSET=utf8;";
                
    if($BD->prepare($t_prevues)->execute()){
        echo "<p> Création de la table ".TABLE_HEURES_PREVUES." réussie. </p>";
    }
    else {
        $erreur = true;
        echo "<p> Problème lors de la création de la table ".TABLE_HEURES_PREVUES.". </p>";   
    }

     echo "<h1>Création de la table ".TABLE_HEURES_REELLES ."</h1>";
    
    // Création de la table Lieux

    $t_reelles = "CREATE  TABLE ".TABLE_HEURES_REELLES."(
                id INT AUTO_INCREMENT PRIMARY KEY,
                jour date  NOT NULL,
                debut datetime NOT NULL,
                fin datetime NOT NULL,
                id_nature INT NOT NULL,
                id_agent INT NOT NULL,
                id_lieu INT NOT NULL,
                id_type INT NOT NULL,
                CONSTRAINT reelles_nature_fk FOREIGN KEY (id_nature)
                REFERENCES ".TABLE_NATURE."(id),
                CONSTRAINT reelles_agent_fk FOREIGN KEY (id_agent)
                REFERENCES ".TABLE_AGENTS."(id),
                CONSTRAINT reelles_lieu_fk FOREIGN KEY (id_lieu)
                REFERENCES ".TABLE_LIEUX."(id),
                CONSTRAINT reelles_type_fk FOREIGN KEY (id_type)
                REFERENCES ".TABLE_TYPE."(id)
                ) 
                ENGINE=InnoDb DEFAULT CHARSET=utf8;";
                
    if($BD->prepare($t_reelles)->execute()){
        echo "<p> Création de la table ".TABLE_HEURES_REELLES." réussie. </p>";
    }
    else {
        $erreur = true;
        echo "<p> Problème lors de la création de la table ".TABLE_HEURES_REELLES.". </p>";   
    }



}

?>
  </body>
 </html>