<?php
// Configuration pour la base de données
define("BD_HOST", "localhost");
define("BD_BASE", "Gestion_Agents");
define("BD_USER", "root");
define("BD_PASSWORD", "root");
define("TABLE_AGENTS", "agents");
define("TABLE_LIEUX", "lieux");
define("TABLE_CONTRATS", "contrats");
define("TABLE_ROLE", "roles");
define("TABLE_HEURES_PREVUES", "heuresPrevues");
define("TABLE_TYPE", "type");
define("TABLE_HEURES_REELLES", "heuresReelles");
define("TABLE_TACHE", "tache");
define("TABLE_NATURE", "nature");
define("TABLE_PERIODE", "periode");